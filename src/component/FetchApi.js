const keyValue = "eae633dae6fa76a149ad9223c1a2c552";
const tokenValue = "ATTA07a64511c9fd8ab2e80abcea1105c3e240d0b1e1cd98104ccf3a0f990634655d0734081C";

import axios from "axios";

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.data;
  } else {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
}

export function FetchApi(boardId) {
  return axios
    .get(`https://api.trello.com/1/boards/${boardId}/lists?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("There was an error fetching the board!", error);
      throw error;
    });
}

export function getAllBoard() {
  return axios
    .get(`https://api.trello.com/1/members/me/boards?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("There was an error fetching all boards!", error);
      throw error;
    });
}

export function createBoard(nameOfBoard) {
  return axios
    .post(`https://api.trello.com/1/boards/?name=${nameOfBoard}&key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("There was an error creating the board!", error);
      throw error;
    });
}

export function getCards(listId) {
  return axios
    .get(`https://api.trello.com/1/lists/${listId}/cards?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("Error getting card info:", error.message);
      throw error;
    });
}

export function createCards(cardName, listId) {
  return axios
    .post(`https://api.trello.com/1/cards?key=${keyValue}&token=${tokenValue}`, { name: cardName, idList: listId })
    .then(checkStatus)
    .catch((error) => {
      console.error("There was an error creating the card!", error);
      throw error;
    });
}

export function createLists(listName, boardId) {
  return axios
    .post(`https://api.trello.com/1/lists?key=${keyValue}&token=${tokenValue}`, { name: listName, idBoard: boardId })
    .then(checkStatus)
    .catch((error) => {
      console.error("Error creating the list!", error.message);
      throw error;
    });
}

export function deleteList(listId) {
  return axios
    .put(`https://api.trello.com/1/lists/${listId}/closed?key=${keyValue}&token=${tokenValue}`, { value: true })
    .then(checkStatus)
    .catch((error) => {
      console.error("Error deleting the list!", error.message);
      throw error;
    });
}

export function deleteCard(cardid) {
  return axios
    .delete(`https://api.trello.com/1/cards/${cardid}?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("Error deleting the card!", error.message);
      throw error;
    });
}

export function getCheckList(cardId) {
  return axios
    .get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("Error getting the checklist!", error.message);
      throw error;
    });
}

export function createCheckList(cardId, checkListName) {
  return axios
    .post(`https://api.trello.com/1/checklists?idCard=${cardId}&key=${keyValue}&token=${tokenValue}`, { name: checkListName })
    .then(checkStatus)
    .catch((error) => {
      console.error("Error creating the checklist!", error.message);
      throw error;
    });
}

export function deleteCheckList(checkListId) {
  return axios
    .delete(`https://api.trello.com/1/checklists/${checkListId}?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("Error deleting the checklist!", error.message);
      throw error;
    });
}

export function getCheckItems(checkListId) {
  return axios
    .get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("Error getting the check items!", error.message);
      throw error;
    });
}

export function createCheckItem(checkListId, checkItemName) {
  return axios
    .post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("Error creating the check item!", error.message);
      throw error;
    });
}

export function deleteCheckItem(checkListId, idCheckItem) {
  return axios
    .delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${idCheckItem}?key=${keyValue}&token=${tokenValue}`)
    .then(checkStatus)
    .catch((error) => {
      console.error("Error deleting the check item!", error.message);
      throw error;
    });
}

export function changeItemCheckbox(cardId, checkItemId, newState) {
  return axios
    .put(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${keyValue}&token=${tokenValue}`, { state: newState })
    .then(checkStatus)
    .catch((error) => {
      console.error("Error changing the checkbox state!", error.message);
      throw error;
    });
}
